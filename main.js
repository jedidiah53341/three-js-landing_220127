import './style.css';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

// Setup
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector('#bg'),
});
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
camera.position.setZ(-20);
renderer.render(scene, camera);

// Torus
//const geometry = new THREE.TorusGeometry(10, 3, 16, 100);
//const material = new THREE.MeshStandardMaterial({ color: 0xff6347 });
//const torus = new THREE.Mesh(geometry, material);
//scene.add(torus);

// Circle
const geometry1 = new THREE.CircleGeometry( 5, 40 );
const material1 = new THREE.TextureLoader().load('1.png');
const circle = new THREE.Mesh( geometry1, new THREE.MeshBasicMaterial({ map: material1, transparent: true, opacity: 0.7 }) );
circle.position.x = 4;
circle.position.y = -1;
circle.position.z = -5;
circle.rotation.x = 0;
scene.add(circle);

// Circle 2
const geometry2 = new THREE.CircleGeometry( 5, 40 );
const material2 = new THREE.TextureLoader().load('2.png');
const circle2 = new THREE.Mesh( geometry2, new THREE.MeshBasicMaterial({ map: material2, transparent: true, opacity: 1 }) );
circle2.position.x = -9;
circle2.position.y = 10;
circle2.position.z = -30;
circle2.rotation.y = 0.2;
circle2.scale.set(6,6,6)
scene.add(circle2);

// Circle 3
const geometry3 = new THREE.CircleGeometry( 5, 40 );
const material3 = new THREE.TextureLoader().load('3.png');
const circle3 = new THREE.Mesh( geometry2, new THREE.MeshBasicMaterial({ map: material3, transparent: true, opacity: 1 }) );
circle3.position.x = -10;
circle3.position.y = -10;
circle3.position.z = -10;
circle3.rotation.y = 0.2;
circle3.rotation.x = 5.2;
circle3.scale.set(2,2,2)
scene.add(circle3);

// Lights
const pointLight = new THREE.PointLight(0xffffff);
pointLight.position.set(5, 5, 5);
const ambientLight = new THREE.AmbientLight(0xffffff);
scene.add(pointLight, ambientLight);

// Background
const spaceTexture = new THREE.TextureLoader().load('space.jpg');
scene.background = spaceTexture;

// Scroll Animation

function moveCamera() {
  const t = document.body.getBoundingClientRect().top;

  camera.position.z = t * -0.005;
  camera.rotation.y = t * -0.00005;
}

function moveCircle() {
  const t = document.body.getBoundingClientRect().top;

  circle2.rotation.z = t * -0.5;
  circle2.rotation.x = t * -0.5;
  circle2.rotation.y = t * -0.5;
}

document.body.onscroll = moveCircle;
document.body.onscroll = moveCamera;
moveCamera();

// Animation Loop

function animate() {
  requestAnimationFrame(animate);

  //torus.rotation.x += 0.01;
  //torus.rotation.y += 0.005;
  //torus.rotation.z += 0.01;

  circle.rotation.z += 0.03;
  circle2.rotation.z += 0.002;
  circle3.rotation.z += 0.002;

  // controls.update();

  renderer.render(scene, camera);
}

animate();
