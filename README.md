# three-js-landing_220127
A landing page with background animations made with three.js . Unfortunately i think it doesn't work very well on mobile devices -- thus i tried to keep the animations simple. Loading heavier animations caused my mobile browser to freeze and crash. It looks good on desktop, though. I think three.js is a good alternative to svg animations.
